def vec(x, y, z):
    """return a numpy.array with type numpy.float"""


def normalize(v):
    """return a normalized vector in direction v"""


def reflect(v, normal):
    """return v reflected through normal"""
