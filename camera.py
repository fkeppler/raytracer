from ray import *
class AbstractCamera():

    def ray(self, x, y):
        return Ray((0, 0, 0), (0, 0, -1))


class Camera(AbstractCamera):

    def __init__(self,
                 eye=(0, 0, 10),
                 ul = (-10, 10, -10),
                 ur = (10, 10, -10),
                 ll = (-10, -10, -10),
                 lr = (10, -10, -10)):

        self.eye = eye
        self.ul = ul
        self.ur = ur
        self.ll = ll
        self.lr = lr
        self.height = 512
        self.width = 512
        self.depth = -.5
        
    def setScreen(self, height, width, depth=-.5):
        "initializes the screen height and width"
        self.height = height
        self.width = width
        self.depth = depth
        
    def ray(self, x, y):
        #CS is a very precise art
        #Ray(self.camera.eye, vec(x,y,-.5))
        xcomp = x - .5
        ycomp = y - .5
        zcomp = self.depth
        newray = Ray(self.eye, (2*xcomp, 2*ycomp, zcomp))
        #print(newray.vector)
        return newray
        
#    def ray(self, x, y):
#        #heavily assumes screen is positioned at default
#        xcomp = -10 + 20*x/self.width
 #      ycomp = 10 - 20*y/self.height
  #      zcomp = -20        
   #     return Ray(self.eye, vec(xcomp, ycomp, zcomp))
