# main.py for raytracer2014
# Geoffrey Matthews
import os
import pygame
from pygame.locals import *
from world import *
from camera import *
from shape import *
from light import *
from ray import *
from material import *
if __name__ == "__main__":
    main_dir = os.getcwd()
else:
    main_dir = os.path.split(os.path.abspath(__file__))[0]
data_dir = os.path.join(main_dir, 'data')


def handleInput(screen):
    for event in pygame.event.get():
        if event.type == QUIT:
            return True
        elif event.type == KEYDOWN:
            if event.key == K_ESCAPE:
                return True
            elif event.key == K_s:
                pygame.event.set_blocked(KEYDOWN | KEYUP)
                fname = raw_input("File name? ")
                pygame.event.set_blocked(0)
                pygame.image.save(screen, fname)
    return False


def main():
    pygame.init()
    screen = pygame.display.set_mode((1024, 1024))
    pygame.display.set_caption('Raytracing!')
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((1, 0, 0))
    screen.blit(background, (0, 0))
    pygame.display.flip()
    going = True
    pixelsize = 128  # power of 2
    width, height = screen.get_size()
    print(width, height)#DEBUG
    world = World([Sphere((2, 2, .5), 3, Phong(color=(1, 0, 0))),
                   Sphere((2, -2, 0), 3, Phong(color=(0, 1, 0))),
                   Sphere((-2, 0, -.5), 3, Phong(color=(0, 0, 1)))],
                  Light(),
                  Camera((0,0,10)))
#    world = ThreeSpheres()
#    world = RandomSpheres(n=32)
    #world = World()
    world.camera.setScreen(height, width)
    while going:
        going = not(handleInput(screen))
        while pixelsize > 0:
            for x in range(0, width, pixelsize):
                xx = x / float(width)
                for y in range(0, height, pixelsize):
                    # clock.tick(2)
                    yy = y / float(height)
                    # draw into background surface
                    color = world.colorAt(xx, yy)
                    color = [int(255 * c) for c in color]
                    for i in range(0,3):#fix some rounding errors
                        if color[i] > 255:
                            #print(color[i])
                            #print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
                            color[i] = 255
                    r, g, b = color
                    #print(r,g,b)#debug
                    try:
                        color = pygame.Color(r, g, b, 255)  # .correct_gamma(1/2.2)
                    except:
                        print("rgb values:")
                        print(r,g,b)
                    background.fill(color, ((x, y), (pixelsize, pixelsize)))
                if handleInput(screen):
                    return
            # draw background into screen
            screen.blit(background, (0, 0))
            pygame.display.flip()
            print(pixelsize)#DEBUG
            pygame.time.wait(1000)
            pixelsize /= 2
# if this file is executed, not imported
if __name__ == '__main__':
    try:
        main()
    finally:
        pygame.quit()
