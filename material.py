from vec import *
def calcColor(ambColor, ambCoeff, diffColor, diffCoeff, normal, lightColor, lightVector, specColor, specCoeff, reflection, view, shiny):
    number1 = ambColor*ambCoeff*diffColor
    number2 = diffCoeff*diffColor*abs(dot(normal, lightVector))
    dot2 = dot(reflection, view)
    number3 =  specCoeff*specColor*dot2**shiny
    #number3 = 0#debug
    answer = number1 + lightColor*(number2+number3)
    #print(number1, number2, number3, answer)#debug
    return answer

    
class Material():

    def colorAt(self, point, normal, ray, world):
        return vec(1, 1, 1)


class Flat(Material):

    def __init__(self, color=(0.25, 0.5, 1.0)):
        self.color = color
        
        
class Phong(Material):

    def __init__(self,
                    color=(0.25, 0.5, 1.0),
                    specularColor=(1, 1, 1),
                    ambient = 0.2,
                    diffuse = 1.0,
                    specular = 0.5,
                    shiny = 64):
        
        self.color = color
        self.specularColor = specularColor
        self.ambient = ambient
        self.diffuse = diffuse
        self.specular = specular
        self.shiny = shiny
        
    def colorAt(self, point, normal, ray, world):
        R = 0
        G = 1
        B = 2
        view = normalize(point-world.camera.eye)
        light = world.lights.point
        lightv = normalize(point-light)
        reflection = normalize(normal*2*dot(normal, light) - light)
        redPart = calcColor(world.neutral[R], self.ambient, self.color[R], self.diffuse, normal, world.lights.color[R], lightv, self.specularColor[R], self.specular, reflection, view, self.shiny)
        
        bluePart = calcColor(world.neutral[B], self.ambient, self.color[B], self.diffuse, normal, world.lights.color[B], lightv, self.specularColor[B], self.specular, reflection, view, self.shiny)
        
        greenPart = calcColor(world.neutral[G], self.ambient, self.color[G], self.diffuse, normal, world.lights.color[G], lightv, self.specularColor[G], self.specular, reflection, view, self.shiny)
        
        return vec(redPart, greenPart, bluePart)
