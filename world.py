from ray import *
from camera import *
from light import *
from vec import *
class AbstractWorld():
    def colorAt(self, x, y):
        pass


class World(AbstractWorld):

    def __init__(self,
                 objects=None,
                 lights=Light(),
                 camera=Camera(),
                 maxDepth=0,
                 neutral=(.5, .5, .5),
                 nsamples = 1,
                 gamma = 1):
        self.objects = objects
        self.lights = lights
        self.camera = camera
        self.maxDepth = maxDepth
        self.neutral = neutral
        self.nsamples = nsamples
        self.gamma = gamma
        
    def colorAt(self, x, y):
        #vec1 = Ray(self.camera.eye, vec(x,y,-.5))
        vec1 = self.camera.ray(x,y)
        collision = vec1.closestHit(self)
        if collision == None:
            return self.neutral
        #print("collision with object")#DEBUG
        obj = collision[3]
        collisionPoint = collision
        return obj.material.colorAt(collision[1], collision[2], vec1, self) 
        #return (0,1,0)#debug
        
