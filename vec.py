# A simple vector class
#copied from http://svn.python.org/projects/python/trunk/Demo/classes/Vec.py

def vec(*v):
    return Vec(*v)

def pointDist(v1, v2):
    "gives the distance between two 3d points"
    if len(v1) != len(v2):
        raise TypeError("vectors must be same size!")
    answer = 0;
    for x in range(0, len(v1)):
        answer = answer + (v1[x]-v2[x])**2
    return answer**.5
    
def dot(v1, v2):
    "the dot product of two vectors"
    answer = []
    if len(v1) != len(v2):
        raise TypeError("vectors must be same size!")
    for x in range(0, len(v1)):
        answer.append(v1[x]*v2[x])
    return sum(answer)

def Normalize (vector):
    #turn a vector into a unit vector
    x = vector[0]
    y = vector[1]
    z = vector[2]
    magnitude = ((x**2) + (y**2) + (z**2))**.5
    if magnitude == 0:
        return vec(0,0,0)
    return vec(x/magnitude, y/magnitude, z/magnitude)

def normalize(vector):
    return Normalize(vector)

class Vec:

    def __init__(self, *v):
        holder = list(v)
        if len(holder) < 1:
            return
        if isinstance(holder[0], tuple):
            self.v = list(holder[0])
        else:
            self.v = holder
        

    def fromlist(self, v):
        if not isinstance(v, list):
            raise TypeError
        self.v = v[:]
        return self
    

    def __repr__(self):
        return 'vec(' + repr(self.v)[1:-1] + ')'

    def __len__(self):
        return len(self.v)

    def __getitem__(self, i):
        return self.v[i]

    def __add__(self, other):
        # Element-wise addition
        v = map(lambda x, y: x+y, self, other)
        return Vec().fromlist(v)

    def __sub__(self, other):
        # Element-wise subtraction
        #print(self.v)#DEBUG
        v = map(lambda x, y: x-y, self, other)
        return Vec().fromlist(v)

    def __mul__(self, scalar):
        # Multiply by scalar
        v = map(lambda x: x*scalar, self.v)
        return Vec().fromlist(v)
    
