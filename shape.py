import numpy
from vec import *
from material import *
EPSILON = 1.0e-10


class GeometricObject():

    def hit(self, ray):
        """Returns (dist, point, normal, object) if hit
        and t > EPSILON"""
        return (None, None, None, None)


class Sphere(GeometricObject):

    def __init__(self, point=(0,0,0), radius=1,
                 material=None):
        #if not(material):
        #    material = Phong()
        self.point = vec(point)
        self.radius = radius
        self.material = material
        
    def normal(self, point):
        "finds the normal for this sphere at point"
        return normalize(self.point-point) #easy because normal is straight from origin through the edge.
        
    def hit(self, ray):
        #some variables for our quadratic:
        #ray.vector = vec(0,0,-1)
        #ray.point = vec(0,0,10)#debug
        xd = ray.vector[0]
        yd = ray.vector[1]
        zd = ray.vector[2]
        x0 = ray.point[0]
        y0 = ray.point[1]
        z0 = ray.point[2]
                
        A = xd**2 + yd**2 + zd**2
        
        B = 2 * (xd*(x0 - self.point[0]) + yd*(y0-self.point[1]) + zd*(z0-self.point[2]))
        
        C = (x0-self.point[0])**2 + (y0-self.point[1])**2 + (z0-self.point[2])**2 - self.radius**2
        
        discriminant = B**2 - 4*A*C
        #print([ray.vector, ray.point])
        #print([self.point, self.radius])
        #print([A,B,C])
        #print(discriminant)#debug
        #three possible cases based on discriminant.
        if discriminant < 0:
            #there are no solutions, no intersections
        #    print("ray didnt hit")
            return False
#        elif discriminant == 0:
 #           #there is one solution, the exact edge of the sphere
  #          return False
        else:
            #print("ray hits!")#debug
            hight = (0-B - discriminant**.5)/(2*A)
            lowt = (0-B + discriminant**.5)/(2*A)
            dist = min(hight, lowt)
            return (dist, ray.pointAt(dist), self.normal(ray.pointAt(dist)), self)
      
        
