from vec import *
class AbstractRay():

    def pointAt(self, distance):
        """return the point at distance along the ray"""
        return (0, 0, 0)

    def closestHit(self, world):
        """return (dist,point,normal,obj)"""
        return None


class Ray(AbstractRay):

    def __init__(self, point, vector, depth=0):
        self.point = vec(point)
        self.vector = normalize(vector)
        
    def pointAt(self, distance):
        return self.point+(self.vector * distance)
        
    def closestHit(self, world):
        current = None
        holder = None
        for shape in world.objects:
            hit = shape.hit(self)
            if hit:
                #print("found a sphere!")#DEBUG
                if current == None:
                    current = hit
                elif current[0] > hit[0]:
                    current = hit
        return current
